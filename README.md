# GO-MCF

Common helper methods for MCF golang applications

## Base url resolver
This provide a safe approach to concatenate a base url with path string, and resolve the joining slash between the 2

| Parameter | Type | Description |
|---|---|---|
Base URL | String | Scheme + host name of URL
Path | String | Path of URL

```
path := ResolveURL("http://somebasepath", "/path")
fmt.Print(path)
```
> Result: http://somebasepath/path


## Cryptography
Provide the following methods to get and initialize a `Signer` object which is then use to retrieve a authorization token from a token issuing server.

| Parameter | Type | Description |
|---|---|---|
Private key file | String | Location of file containing private key
Service Id | String | ID of requesting service
Authenticator | String | Location of service authenticator
Token Life Time Clock Skew | Int64 | Time to live for request

```
Signer := GetSigner("./resources/private_key.pem", "7ac7681a3e0649ecb9cc1e2f0b3c8a07", "http://authenticator.com", 1000000)

Signer.InitSigner()

token := Signer.GetAuthToken()
```
> Result: token issued by issuing server