package mcf

import "net/url"

// ResolveURL concates base url with extended path
func ResolveURL(base string, path string) (string, error) {
	urlPath, err := url.Parse(path)

	urlBase, err := url.Parse(base)

	requestPath := urlBase.ResolveReference(urlPath)

	return requestPath.String(), err
}
