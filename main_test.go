package mcf

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestResolveURL(t *testing.T) {
	assert := assert.New(t)
	testData := []struct {
		base   string
		path   string
		result string
	}{
		{"http://www.foo.bar", "test/path", "http://www.foo.bar/test/path"},
		{"http://www.foo.bar/", "test/path", "http://www.foo.bar/test/path"},
		{"http://www.foo.bar", "/test/path", "http://www.foo.bar/test/path"},
		{"http://www.foo.bar/", "/test/path", "http://www.foo.bar/test/path"},
	}

	for _, test := range testData {
		t.Run("ResolveURL method", func(t *testing.T) {
			result, _ := ResolveURL(test.base, test.path)
			assert.Equal(test.result, result, "Url output should match")
		})
	}
}
