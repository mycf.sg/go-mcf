package mcf

import (
	"bufio"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/labstack/gommon/log"
)

type (
	// Signer describe the imported private key and token from svc-auth
	Signer struct {
		privateKey   *rsaPrivateKey
		token        *tokenStruct
		cryptography *Cryptography
		Cryptography interface {
			generateNonce() int64
			getPrivateKeyFile() (*os.File, error)
			getRSAKey(rawKey interface{}) (*rsaPrivateKey, error)
		}
	}

	tokenStruct struct {
		jwtToken       string
		expirationTime int64
	}

	rsaPrivateKey struct {
		*rsa.PrivateKey
	}

	// Cryptography describe the utils to import the private key from file
	Cryptography struct {
		keyLocation            string
		serviceID              string
		authenticator          string
		tokenLifeTimeClockSkew int64
	}
)

var (
	osExit   = os.Exit
	logFatal = log.Fatal
)

// GetSigner return a new Signer instance
func GetSigner(privateKeyLoc string, serviceID string, authenticator string, tokenLifeTimeClockSkew int64) *Signer {
	newSigner := &Signer{
		nil,
		nil,
		&Cryptography{privateKeyLoc, serviceID, authenticator, tokenLifeTimeClockSkew},
		&Cryptography{}, //Interfaces
	}
	return newSigner
}

// GetAuthToken returns access token
func (r *Signer) GetAuthToken() string {
	isTokenValid := r.checkTokenExpiry()

	if !isTokenValid {
		r.getJWTToken()
	}

	return "Bearer " + r.token.jwtToken
}

// InitSigner initial Signer with private key
func (r *Signer) InitSigner() {
	privateKey, err := r.importPrivateKey()

	if err != nil {
		log.Error("from initSigner")
		logFatal(err)
		osExit(1)
	}

	token := &tokenStruct{}

	r.privateKey = privateKey
	r.token = token
}

// Open file containing private key
func (c *Cryptography) getPrivateKeyFile() (*os.File, error) {
	privateKeyFile, err := os.Open(c.keyLocation)
	return privateKeyFile, err
}

// Retrieve RSA Key from raw key string
func (c *Cryptography) getRSAKey(rawKey interface{}) (*rsaPrivateKey, error) {
	var key *rsaPrivateKey
	switch t := rawKey.(type) {
	case *rsa.PrivateKey:
		key = &rsaPrivateKey{t}
	default:
		return nil, fmt.Errorf("Unsupported key type %T", t)
	}
	return key, nil
}

// Generate a timestamp to be use as a nonce token
func (c *Cryptography) generateNonce() int64 {
	nonce := time.Now().UTC().UnixNano() / 1e6

	return nonce
}

// Import private key from file
func (r *Signer) importPrivateKey() (*rsaPrivateKey, error) {
	var rawKey interface{}

	privateKeyFile, err := r.cryptography.getPrivateKeyFile()

	if err != nil {
		return nil, err
	}

	pemfileinfo, _ := privateKeyFile.Stat()
	size := pemfileinfo.Size()
	pembytes := make([]byte, size)
	buffer := bufio.NewReader(privateKeyFile)
	buffer.Read(pembytes)
	block, _ := pem.Decode(pembytes)
	privateKeyFile.Close()

	switch block.Type {
	case "PRIVATE KEY":
		key, err := x509.ParsePKCS8PrivateKey(block.Bytes)
		if err != nil {
			return nil, err
		}
		rawKey = key
	default:
		return nil, fmt.Errorf("Error parsing private key %s", block)
	}

	rsaKey, err := r.Cryptography.getRSAKey(rawKey)

	return rsaKey, err
}

// Sign payload with RSA private key
func (r *Signer) sign(data []byte) ([]byte, error) {
	h := sha256.New()
	h.Write(data)
	d := h.Sum(nil)
	return rsa.SignPKCS1v15(rand.Reader, r.privateKey.PrivateKey, crypto.SHA256, d)
}

// Check JWT token's validity
func (r *Signer) checkTokenExpiry() bool {
	currentTime := r.Cryptography.generateNonce()
	return r.token.expirationTime > currentTime
}

// GetJWTToken retrieve JWT token
func (r *Signer) getJWTToken() {
	var (
		nonce       = r.Cryptography.generateNonce()
		nonceString = strconv.FormatInt(nonce, 10)
		payload     = []byte(r.cryptography.serviceID + nonceString)
	)

	signature, err := r.sign(payload)

	if err != nil {
		log.Errorf("The signing of the payload failed with error: %s", err)
	}

	client := &http.Client{}
	req, _ := http.NewRequest("GET", r.cryptography.authenticator, nil)

	// Set request headers
	req.Header.Set("username", r.cryptography.serviceID)
	req.Header.Set("signature", base64.StdEncoding.EncodeToString(signature))
	req.Header.Set("nonce", nonceString)

	res, err := client.Do(req)

	if err != nil {
		log.Errorf("The HTTP request failed with error: %s", err)
	}

	if res.StatusCode == http.StatusOK {
		body, err := ioutil.ReadAll(res.Body)

		if err != nil {
			log.Errorf("Unable to read response body: %s", err)
		}

		r.token.jwtToken = string(body)
		r.token.expirationTime = nonce + r.cryptography.tokenLifeTimeClockSkew
	} else {
		body, err := ioutil.ReadAll(res.Body)
		log.Errorf("Request forbidden - Status: %s", strconv.Itoa(res.StatusCode))
		log.Errorf("Body: %s", string(body))
		log.Errorf("Error: %s", err)
	}
	defer res.Body.Close()
}
