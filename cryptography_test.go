package mcf

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type (
	CryptographyMock struct {
		mock.Mock
	}

	mockServerHandler struct {
		new   func(string) string
		close func()
	}

	mockServer struct {
		server  *httptest.Server
		handler *mockServerHandler
	}
)

var (
	validPemString = `-----BEGIN PRIVATE KEY-----
MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAvqud4Rcne4rnBMD2
ePmaREGTa8AelnOEPcqsIUHTKgAVmX3IpHHo0N0UQsg7JUbGgvOwPC6Yh6RuyKbn
GNgnwwIDAQABAkEAkWdavN7lcydYSZ3F2Dew10CS5eba7K0nOeAnKC2ElU+ss3fc
UFvpyvtrBJ9hkt8CPOs/vh1b9CyhWXcEGfJFYQIhAPK2HzGYkq3P2jX3hHC/0KBI
4wZjz1UkLxUk84WhwDS7AiEAyRwUiJm5/az9YzS+zj853W4B4zJ8xv3pbuA2kFlx
rJkCIQDBbRz9BI/Yzy4jFihJRsS4CH5NCFwiiVywaCtfEyI/owIgMXzudJrXkNwD
0mjFVz1pwNp0LnNbDyCmU7lq0+Seu2kCIAEfli2GcAmXuvLLKDdJylUjTFHHcSeo
hO/KHpv7KsJb
-----END PRIVATE KEY-----`
)

func (mock *CryptographyMock) generateNonce() int64 {
	args := mock.Called()

	return int64(args.Get(0).(int64))
}

func (mock *CryptographyMock) getPrivateKeyFile() (*os.File, error) {
	mock.Called()

	return nil, nil
}

func (mock *CryptographyMock) getRSAKey(rawKey interface{}) (*rsaPrivateKey, error) {
	mock.Called()

	return nil, nil
}

func getTestRsaKey() interface{} {
	pemString := validPemString

	block, _ := pem.Decode([]byte(pemString))
	rawKey, _ := x509.ParsePKCS8PrivateKey(block.Bytes)

	return rawKey
}

func getTestPrivateKey() *rsa.PrivateKey {
	rawKey := getTestRsaKey()
	key := rawKey.(*rsa.PrivateKey)
	return key
}

func TestGetSigner(t *testing.T) {
	assert := assert.New(t)

	mockSigner := &Signer{
		nil,
		nil,
		&Cryptography{"foo", "bar", "baxz", 1800000},
		&Cryptography{},
	}
	signer := GetSigner("foo", "bar", "baxz", 1800000)

	assert.Equal(signer, mockSigner, "newSigner() did not return an instance of Signer struct")
}

func TestCheckTokenExpiry(t *testing.T) {
	var isTokenValid bool
	assert := assert.New(t)
	cryptographyMock := &CryptographyMock{}
	mockSigner := &Signer{
		nil,
		&tokenStruct{},
		&Cryptography{},
		cryptographyMock,
	}

	cryptographyMock.On("generateNonce").Return(int64(150000000000))

	t.Run("JWT token expired", func(t *testing.T) {
		mockSigner.token.expirationTime = 149999999999
		isTokenValid = mockSigner.checkTokenExpiry()
		assert.False(isTokenValid, "Token should be invalid")
	})

	t.Run("JWT token has not expire", func(t *testing.T) {
		mockSigner.token.expirationTime = 150000000001
		isTokenValid = mockSigner.checkTokenExpiry()
		assert.True(isTokenValid, "Token should be valid")
	})
}

func TestSign(t *testing.T) {
	assert := assert.New(t)
	expectedSignature := "m7yKIISKHUDiSqDj96zmmNVtHQF/+NDPXly2rx/XozCpEeVwXkxpkTL1oWT2Ig6PvorEiw2ucQYbTlqFndlsJQ=="

	mockSigner := &Signer{}

	mockSigner.privateKey = &rsaPrivateKey{
		PrivateKey: getTestPrivateKey(),
	}

	t.Run("generate valid signature", func(t *testing.T) {
		signature, _ := mockSigner.sign([]byte("foobar"))
		assert.Equal(base64.StdEncoding.EncodeToString(signature), expectedSignature, "Signature should match")
	})

	t.Run("generate invalid signature", func(t *testing.T) {
		signature, _ := mockSigner.sign([]byte("baxz"))
		assert.NotEqual(base64.StdEncoding.EncodeToString(signature), expectedSignature, "Signature should not match")
	})
}

func TestGetAuthToken(t *testing.T) {
	assert := assert.New(t)
	cryptographyMock := &CryptographyMock{}

	var (
		expectedNonce     int64 = 150000000011
		expectedSignature       = "WMcH06BQKUdNjuGHO12Tn3q8ALiPjOxzvTEVrfOJGJ5ga6qyZDVrp3s61mkJXgJTywwtzmurOyCfVjnR3NiZFg=="
	)
	// Setup mock server for svc-auth
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		path := r.URL.EscapedPath()
		username := r.Header.Get("username")
		signature := r.Header.Get("signature")
		nonce := r.Header.Get("nonce")

		assert.Equal("/mcf/login", path, "Endpoint path is incorrect")
		assert.Equal("bazx", username, "Username should match request header")
		assert.Equal(expectedSignature, signature, "SIgnature should match request header")
		assert.Equal("150000000000", nonce, "Nonce should match request header")
		w.Write([]byte("new.access.token"))
	}))
	defer ts.Close()

	mockSigner := &Signer{
		nil,
		&tokenStruct{},
		&Cryptography{"./test.pem", "bazx", ts.URL + "/mcf/login", 11},
		&Cryptography{},
	}

	mockSigner.InitSigner()
	mockSigner.Cryptography = cryptographyMock // Swap interfaces with mock interfaces

	cryptographyMock.On("generateNonce").Return(int64(150000000000))

	t.Run("JWT token has not expired", func(t *testing.T) {
		mockSigner.token.jwtToken = "access.token"
		mockSigner.token.expirationTime = 150000000001
		authToken := mockSigner.GetAuthToken()
		assert.Equal("Bearer access.token", authToken, "Access token should match")
	})

	t.Run("JWT token expired", func(t *testing.T) {
		mockSigner.token.jwtToken = "access.token.expired"
		mockSigner.token.expirationTime = 149999999999
		authToken := mockSigner.GetAuthToken()
		assert.Equal("Bearer new.access.token", authToken, "Access token should match")
	})

	assert.Equal("new.access.token", mockSigner.token.jwtToken, "Access token should match")
	assert.Equal(expectedNonce, mockSigner.token.expirationTime, "Nonce should match")

}

func TestInitSigner(t *testing.T) {
	assert := assert.New(t)

	t.Run("InitSigner successful", func(t *testing.T) {
		signer := GetSigner("./test.pem", "bar", "baxz", 1800000)
		signer.InitSigner()
		testRsaKey := getTestPrivateKey()
		token := &tokenStruct{}

		assert.Equal(testRsaKey, signer.privateKey.PrivateKey, "RSA Key from file does not match test RSA key")
		assert.Equal(token, signer.token, "Token instance does not match")
	})

	t.Run("InitSigner unsuccessful", func(t *testing.T) {

		oldOsExit := osExit
		oldLogFatal := logFatal
		defer func() {
			osExit = oldOsExit
			logFatal = oldLogFatal
		}()

		receivedExitCode := 0
		testOsExit := func(code int) {
			receivedExitCode = code
		}

		testLogFatal := func(i ...interface{}) {
			t.Log(i)
		}

		osExit = testOsExit
		logFatal = testLogFatal

		signer := GetSigner("", "bar", "baxz", 1800000)
		signer.InitSigner()

		assert.Equal(receivedExitCode, 1, "Token instance does not match")
	})
}
